#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a

from devnet.models import Department, Navbar, Devicedb, MonitorInterval
from django.shortcuts import render
import json
from datetime import datetime, timedelta
from django.contrib.auth.decorators import permission_required


def get_cpu_monitor_interval():
    try:
        cpu_monitor_interval = MonitorInterval.objects.get(name='cpu_interval').interval
    except MonitorInterval.DoesNotExist:
        m = MonitorInterval(name='cpu_interval',
                            interval=1)
        m.save()
        cpu_monitor_interval = MonitorInterval.objects.get(name='cpu_interval').interval
    return cpu_monitor_interval


@permission_required('devnet.view_devicecpu')
def device_monitor_cpu(request):
    html_title = Department.objects.get(name='admin').departtitle.title
    navbar_list = []
    for navbar in Navbar.objects.all().order_by('id'):
        navbar_list.append([navbar.name, navbar.url])
    active_navbar = '设备监控'
    sidebar_list = []
    for sidebar in Navbar.objects.get(name='设备监控').Sidebar.all().order_by('id'):
        sidebar_list.append([sidebar.name, sidebar.url])
    active_sidebar = 'CPU利用率'

    devices_list = []
    for device in Devicedb.objects.all().order_by('id'):
        devices_list.append({'id': device.id, 'name': device.name})

    current_obj = Devicedb.objects.all().order_by('id')[0]
    current = current_obj.name

    cpu_usage_in_monitor_interval = current_obj.cpu_usage.filter(record_datetime__gt=datetime.now() - timedelta(hours=get_cpu_monitor_interval()))

    cpu_usage = []
    cpu_record_time = []

    for x in sorted(cpu_usage_in_monitor_interval, key=lambda k: k.record_datetime):
        cpu_usage.append(x.cpu_usage)  # 把每一分钟采集到的CPU利用率写入cpu_data清单
        # 把采集时间格式化然后写入cpu_time清单
        cpu_record_time.append(x.record_datetime.strftime('%H:%M'))
        # 返回'monitor_devices_cpu.html'页面,与设备清单, 当前设备, CPU利用率清单cpu_data, CPU采集时间清单cpu_time
        # 由于数据会被JavaScript使用, 所以需要使用JSON转换为字符串
    cpu_data = json.dumps(cpu_usage)
    cpu_time = json.dumps(cpu_record_time)
    return render(request, 'devnet_device_monitor_cpu.html', locals())


@permission_required('devnet.view_devicecpu')
def device_monitor_cpu_device(request, device_id):
    html_title = Department.objects.get(name='admin').departtitle.title
    navbar_list = []
    for navbar in Navbar.objects.all().order_by('id'):
        navbar_list.append([navbar.name, navbar.url])
    active_navbar = '设备监控'
    sidebar_list = []
    for sidebar in Navbar.objects.get(name='设备监控').Sidebar.all().order_by('id'):
        sidebar_list.append([sidebar.name, sidebar.url])
    active_sidebar = 'CPU利用率'

    devices_list = []
    for device in Devicedb.objects.all().order_by('id'):
        devices_list.append({'id': device.id, 'name': device.name})

    current_obj = Devicedb.objects.get(id=device_id)
    current = current_obj.name

    cpu_usage_in_monitor_interval = current_obj.cpu_usage.filter(record_datetime__gt=datetime.now() - timedelta(hours=get_cpu_monitor_interval()))

    cpu_usage = []
    cpu_record_time = []

    for x in sorted(cpu_usage_in_monitor_interval, key=lambda k: k.record_datetime):
        cpu_usage.append(x.cpu_usage)  # 把每一分钟采集到的CPU利用率写入cpu_data清单
        # 把采集时间格式化然后写入cpu_time清单
        cpu_record_time.append(x.record_datetime.strftime('%H:%M'))
        # 返回'monitor_devices_cpu.html'页面,与设备清单, 当前设备, CPU利用率清单cpu_data, CPU采集时间清单cpu_time
        # 由于数据会被JavaScript使用, 所以需要使用JSON转换为字符串
    cpu_data = json.dumps(cpu_usage)
    cpu_time = json.dumps(cpu_record_time)

    return render(request, 'devnet_device_monitor_cpu.html', locals())