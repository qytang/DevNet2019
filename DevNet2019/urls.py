"""DevNet2019 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from devnet.views.devnet_home import home
from devnet.views.devnet_device_mgmt import add_device_type, show_device_type, add_device, show_device
from devnet.views.devnet_device_mgmt import delete_device, edit_device
from devnet.views.devnet_device_mgmt import delete_device_type, edit_device_type
from devnet.views.devnet_system_setting_monitor_interval import monitor_interval, reset_monitor_interval
from devnet.views.devnet_system_setting_database_lifetime import database_lifetime, reset_database_lifetime
from devnet.views.devnet_system_setting_threshold_mail import threshold_mail, reset_threshold_mail
from devnet.views.devnet_device_monitor_cpu import device_monitor_cpu, device_monitor_cpu_device
from devnet.views.devnet_device_monitor_mem import device_monitor_mem, device_monitor_mem_device
from devnet.views.device_monitor_if_speed import device_monitor_if_speed, device_monitor_if_speed_device
from devnet.views.device_monitor_if_speed import device_monitor_if_speed_device_ajax
from devnet.views.device_monitor_if_utilization import device_monitor_if_utilization, device_monitor_if_utilization_device
from devnet.views.device_monitor_if_utilization import device_monitor_if_utilization_device_ajax
from devnet.views.device_config import device_config, device_config_dev, device_config_compare, device_del_config, device_download_config, device_show_config
from devnet.views.devnet_home_data import health_reachable, health_cpu, health_mem
from devnet.views.devnet_netflow import devnet_netflow, netflow_protocol, netflow_top_ip
from devnet.views.devnet_qyt_login import qyt_login, qyt_logout
from devnet.views.devnet_elk import elk


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    path('home/reachable', health_reachable),  # 主页获取设备健康摘要JSON数据的URL链接
    path('home/cpu', health_cpu),  # 主页获取CPU利用率摘要JSON数据的URL链接
    path('home/mem', health_mem),  # 主页获取内存利用率摘要JSON数据的URL链接
    path('device_mgmt/add_device_type', add_device_type),
    path('device_mgmt/show_device_type', show_device_type),
    path('device_mgmt/add_device', add_device),
    path('device_mgmt/show_device', show_device),
    path('device_mgmt/deletedevice/<int:device_id>', delete_device),
    path('device_mgmt/editdevice/<int:device_id>', edit_device),
    path('device_mgmt/deletedevicetype/<int:device_type_id>', delete_device_type),
    path('device_mgmt/editdevicetype/<int:device_type_id>', edit_device_type),
    path('system_setting/monitor_interval', monitor_interval),
    path('system_setting/reset_monitor_interval', reset_monitor_interval),
    path('system_setting/database_lifetime', database_lifetime),
    path('system_setting/reset_database_lifetime', reset_database_lifetime),
    path('system_setting/threshold_mail', threshold_mail),
    path('system_setting/reset_threshold_mail', reset_threshold_mail),
    path('device_monitor/cpu', device_monitor_cpu),
    path('device_monitor/cpu/<int:device_id>', device_monitor_cpu_device),
    path('device_monitor/mem', device_monitor_mem),
    path('device_monitor/mem/<int:device_id>', device_monitor_mem_device),
    path('device_monitor/if_speed', device_monitor_if_speed),
    path('device_monitor/if_speed/<int:device_id>', device_monitor_if_speed_device),
    path('device_monitor/if_speed/<int:interface_id>/<str:direction>', device_monitor_if_speed_device_ajax),
    path('device_monitor/if_utilization', device_monitor_if_utilization),
    path('device_monitor/if_utilization/<int:device_id>', device_monitor_if_utilization_device),
    path('device_monitor/if_utilization/<int:interface_id>/<str:direction>', device_monitor_if_utilization_device_ajax),
    path('device_config/', device_config),  # 设备配置备份默认页面
    path('device_config/<int:device_id>', device_config_dev),  # 特定设备配置备份页面
    path('device_config/delete/<int:device_id>/<int:id>', device_del_config),  # 删除特定设备,特定配置备份URL链接
    path('device_config/show/<int:device_id>/<int:id>', device_show_config),  # 查看特定设备,特定配置备份页面
    path('device_config/download/<int:device_id>/<int:id>', device_download_config),   # 下载特定设备,特定配置备份URL链接
    path('device_config/compare/<int:device_id>/<int:id1>/<int:id2>', device_config_compare),  # 比较设备配置备份页面
    path('netflow', devnet_netflow),  # Netflow信息
    path('netflow/protocol', netflow_protocol),  # TOP协议
    path('netflow/top_ip', netflow_top_ip),  # TOP IP
    path('elk', elk),  # ELK整合
    path('accounts/login/', qyt_login),  # 登录页面
    path('accounts/logout/', qyt_logout),  # 注销页面
]
