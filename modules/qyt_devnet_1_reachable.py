#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a

from qyt_devnet_0_snmp_get import snmp_sure_reachable
from qyt_devnet_0_ssh import ssh_sure_shell_login
import django
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'DevNet2019.settings')
django.setup()
from devnet.models import Devicedb, DeviceReachable


def sure_reachable():
    for device in Devicedb.objects.all():
        snmp_reachable = snmp_sure_reachable(device.ip, device.snmp_ro_community)
        if device.enable_password:
            ssh_reachable = ssh_sure_shell_login(device.ip,
                                                 device.type.type_name,
                                                 device.ssh_username,
                                                 device.ssh_password,
                                                 device.enable_password)
        else:
            ssh_reachable = ssh_sure_shell_login(device.ip,
                                                 device.type.type_name,
                                                 device.ssh_username,
                                                 device.ssh_password)

        d = DeviceReachable(device=device,
                            ssh_reachable=ssh_reachable,
                            snmp_reachable=snmp_reachable)
        d.save()


if __name__ == '__main__':
    sure_reachable()