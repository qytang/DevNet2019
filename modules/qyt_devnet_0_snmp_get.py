#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a
import django
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'DevNet2019.settings')
django.setup()
from pysnmp.hlapi import *
from devnet.models import Devicedb, SNMPtype
from pysnmp.entity.rfc3413.oneliner import cmdgen


def snmpv2_get(ip, community, oid, port=161):
    # varBinds是列表，列表中的每个元素的类型是ObjectType（该类型的对象表示MIB variable）
    errorindication, errorstatus, errorindex, varbinds = next(
        getCmd(SnmpEngine(),
               CommunityData(community),  # 配置community
               UdpTransportTarget((ip, port)),  # 配置目的地址和端口号
               ContextData(),
               ObjectType(ObjectIdentity(oid))  # 读取的OID
               )
    )
    # 错误处理
    if errorindication:
        print(errorindication)
    elif errorstatus:
        print('%s at %s' % (
            errorstatus,
            errorindex and varbinds[int(errorindex) - 1][0] or '?'
        )
              )
    # 如果返回结果有多行,需要拼接后返回
    result = ""
    for varBind in varbinds:
        result = result + varBind.prettyPrint() # 返回结果！
    # 返回的为一个元组,OID与字符串结果
    return result.split("=")[0].strip(), result.split("=")[1].strip()


def snmpv2_getnext(ip, community, oid, port=161):
    cmdGen = cmdgen.CommandGenerator()

    errorIndication, errorStatus, errorindex, varBindTable = cmdGen.nextCmd(
        cmdgen.CommunityData(community),  # 设置community
        cmdgen.UdpTransportTarget((ip, port)),  # 设置IP地址和端口号
        oid,  # 设置OID
    )
    # 错误处理
    if errorIndication:
        print(errorIndication)
    elif errorStatus:
        print('%s at %s' % (
            errorStatus.prettyPrint(),
            errorindex and varBinds[int(errorindex) - 1][0] or '?'
        )
              )

    result = []
    # varBindTable是个list，元素的个数可能有好多个。它的元素也是list，这个list里的元素是ObjectType，个数只有1个。
    for varBindTableRow in varBindTable:
        for item in varBindTableRow:
            result.append((item.prettyPrint().split("=")[0].strip(), item.prettyPrint().split("=")[1].strip()))
    return result


def snmp_sure_reachable(ip, community):
    try:
        if "SNMPv2-MIB::sysDescr.0" == snmpv2_get(ip, community, "1.3.6.1.2.1.1.1.0", port=161)[0]:
            snmp_reachable = True
        else:
            snmp_reachable = False
    except IndexError:
        snmp_reachable = False
    return snmp_reachable


def get_mem_cpu(ip):
    device = Devicedb.objects.get(ip=ip)
    cpu_usage_oid = device.type.devicesnmp.get(snmp_type=SNMPtype.objects.get(snmp_type='cpu_usage')).oid
    mem_usage_oid = device.type.devicesnmp.get(snmp_type=SNMPtype.objects.get(snmp_type='mem_usage')).oid
    mem_free_oid = device.type.devicesnmp.get(snmp_type=SNMPtype.objects.get(snmp_type='mem_free')).oid

    ro_community = device.snmp_ro_community
    used = int(snmpv2_get(ip, ro_community, mem_usage_oid)[1])

    free = int(snmpv2_get(ip, ro_community, mem_free_oid)[1])

    cpu = int(snmpv2_get(ip, ro_community, cpu_usage_oid)[1])

    return round(float(used/(free + used)) * 100, 2), cpu


def get_interface_info(ip):
    device = Devicedb.objects.get(ip=ip)
    if_name_oid = device.type.devicesnmp.get(snmp_type=SNMPtype.objects.get(snmp_type='if_name')).oid
    if_speed_oid = device.type.devicesnmp.get(snmp_type=SNMPtype.objects.get(snmp_type='if_speed')).oid
    if_state_oid = device.type.devicesnmp.get(snmp_type=SNMPtype.objects.get(snmp_type='if_state')).oid
    if_in_bytes_oid = device.type.devicesnmp.get(snmp_type=SNMPtype.objects.get(snmp_type='if_in_bytes')).oid
    if_out_bytes_oid = device.type.devicesnmp.get(snmp_type=SNMPtype.objects.get(snmp_type='if_out_bytes')).oid

    if device.type.type_name == 'ASA Firewall':
        if_name_list = [x[1].replace("Adaptive Security Appliance '", "").replace("' interface", "") for x in snmpv2_getnext(ip, device.snmp_ro_community, if_name_oid)]
    else:
        if_name_list = [x[1] for x in snmpv2_getnext(ip, device.snmp_ro_community, if_name_oid)]
    if_speed_list = [int(x[1]) for x in snmpv2_getnext(ip, device.snmp_ro_community, if_speed_oid)]
    if_state_list = [int(x[1]) for x in snmpv2_getnext(ip, device.snmp_ro_community, if_state_oid)]
    if_in_bytes_list = [int(x[1]) for x in snmpv2_getnext(ip, device.snmp_ro_community, if_in_bytes_oid)]
    if_out_bytes_list = [int(x[1]) for x in snmpv2_getnext(ip, device.snmp_ro_community, if_out_bytes_oid)]

    if_all = zip(if_name_list, if_state_list, if_speed_list, if_in_bytes_list, if_out_bytes_list)

    if_all_list = []
    for x in if_all:
        if_dict = {'name': x[0],
                   'state': True if x[1] == 1 else False,
                   'speed': x[2],
                   'in_bytes': x[3],
                   'out_bytes': x[4]}
        if_all_list.append(if_dict)
    return if_all_list


if __name__ == '__main__':
    # # print(snmp_sure_reachable('192.168.1.104', 'qytangro'))
    # print(get_mem_cpu('192.168.1.101'))
    print(get_interface_info('192.168.1.104'))