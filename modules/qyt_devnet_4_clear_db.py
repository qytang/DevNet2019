#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a

import django
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'DevNet2019.settings')
django.setup()
from devnet.models import Devicedb, DataBaseLifetime, NetflowClassicSeven
from datetime import datetime, timedelta


def get_cpu_db_lifetime():
    try:
        cpu_db_lifetime = DataBaseLifetime.objects.get(name='cpu_lifetime').lifetime
    except DataBaseLifetime.DoesNotExist:
        c = DataBaseLifetime(name='cpu_lifetime',
                             lifetime=24)
        c.save()
        cpu_db_lifetime = DataBaseLifetime.objects.get(name='cpu_lifetime').lifetime
    return cpu_db_lifetime


def get_mem_db_lifetime():
    try:
        mem_db_lifetime = DataBaseLifetime.objects.get(name='mem_lifetime').lifetime
    except DataBaseLifetime.DoesNotExist:
        m = DataBaseLifetime(name='mem_lifetime',
                             lifetime=24)
        m.save()
        mem_db_lifetime = DataBaseLifetime.objects.get(name='mem_lifetime').lifetime
    return mem_db_lifetime


def get_reachable_db_lifetime():
    try:
        reachable_db_lifetime = DataBaseLifetime.objects.get(name='reachable_lifetime').lifetime
    except DataBaseLifetime.DoesNotExist:
        m = DataBaseLifetime(name='reachable_lifetime',
                             lifetime=24)
        m.save()
        reachable_db_lifetime = DataBaseLifetime.objects.get(name='reachable_lifetime').lifetime
    return reachable_db_lifetime


def get_interface_db_lifetime():
    try:
        interface_db_lifetime = DataBaseLifetime.objects.get(name='interface_lifetime').lifetime
    except DataBaseLifetime.DoesNotExist:
        m = DataBaseLifetime(name='interface_lifetime',
                             lifetime=24)
        m.save()
        interface_db_lifetime = DataBaseLifetime.objects.get(name='interface_lifetime').lifetime
    return interface_db_lifetime


def get_netflow_db_lifetime():
    try:
        netflow_db_lifetime = DataBaseLifetime.objects.get(name='netflow_lifetime').lifetime
    except DataBaseLifetime.DoesNotExist:
        m = DataBaseLifetime(name='netflow_lifetime',
                             lifetime=24)
        m.save()
        netflow_db_lifetime = DataBaseLifetime.objects.get(name='netflow_lifetime').lifetime
    return netflow_db_lifetime


def clear_db():
    for device in Devicedb.objects.all():
        need_clear_reachable_db = device.reachable.filter(record_datetime__lte=datetime.now() - timedelta(hours=get_reachable_db_lifetime()))
        need_clear_reachable_db.delete()

        need_clear_cpu_db = device.cpu_usage.filter(record_datetime__lte=datetime.now() - timedelta(hours=get_cpu_db_lifetime()))
        need_clear_cpu_db.delete()

        need_clear_mem_db = device.mem_usage.filter(record_datetime__lte=datetime.now() - timedelta(hours=get_mem_db_lifetime()))
        need_clear_mem_db.delete()

        interface_all = device.interface.all()
        for x in interface_all:
            need_clear_interface_state_db = x.interface_state.filter(record_datetime__lte=datetime.now() - timedelta(hours=get_interface_db_lifetime()))
            need_clear_interface_state_db.delete()
            need_clear_interface_in_bytes_db = x.interface_in_bytes.filter(record_datetime__lte=datetime.now() - timedelta(hours=get_interface_db_lifetime()))
            need_clear_interface_in_bytes_db.delete()
            need_clear_interface_out_bytes_db = x.interface_out_bytes.filter(record_datetime__lte=datetime.now() - timedelta(hours=get_interface_db_lifetime()))
            need_clear_interface_out_bytes_db.delete()


def clear_netflowdb():
    need_clear_netflow_db = NetflowClassicSeven.objects.filter(record_datetime__lte=datetime.now() - timedelta(hours=get_netflow_db_lifetime()))
    need_clear_netflow_db.delete()


if __name__ == '__main__':
    clear_db()
    clear_netflowdb()
